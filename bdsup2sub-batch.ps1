<# 
    bdsup2sub-batch-powershell-script/bdsup2sub-batch.ps1
    A simple PowerShell script for batch processing files with BDSup2Sub
    Created by: Kai Sackl (https://gitlab.com/cannonball1911), 19.07.2020
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory=$False)]
    [Switch]${load-settings},
    [String]${resolution},
    [String]${fps-target},
    [String]${convert-fps},
    [String]${delay},
    [String]${filter},
    [String]${palette-mode},
    [String]${minimum-time},
    [String]${merge-time},
    [String]${move-in},
    [String]${move-out},
    [String]${move-x},
    [String]${crop-y},
    [String]${alpha-crop},
    [String]${scale},
    [Switch]${export-palette},
    [Switch]${forced-only},
    [String]${force-all},
    [Switch]${swap},
    [Switch]${fix-invisible},
    [String]${palette-file},
    [Switch]${verbose-output},
    [String]${alpha-thr},
    [String]${lum-low-med-thr},
    [String]${lum-med-hi-thr},
    [String]${language},
    [Switch]${use-predef}
)

$newLine = [Environment]::NewLine
$scriptVersion = "v1.0.0"

Write-Host "BDSup2Sub batch processing script ${scriptVersion} by https://gitlab.com/cannonball1911${newLine}"

$scriptRootFolderPath = Split-Path $script:MyInvocation.MyCommand.Path
$inputBatchProcessFolder = "\input"
$outputBatchProcessFolder = "\output"
$bdSup2SubFileName = "BDSup2Sub.jar"
$predefinedArgsFileName = "predefined_args.ini"
$fullInputFolderPath = Join-Path -Path $scriptRootFolderPath -ChildPath $inputBatchProcessFolder
$fullOutputFolderPath = Join-Path -Path $scriptRootFolderPath -ChildPath $outputBatchProcessFolder
$bdSup2SubFilePath = Join-Path -Path $scriptRootFolderPath -ChildPath $bdSup2SubFileName
$fullPredefinedArgsPath = Join-Path -Path $scriptRootFolderPath -ChildPath $predefinedArgsFileName

if(!(Test-Path $fullInputFolderPath)) {
    New-Item -ItemType Directory -Force -Path $fullInputFolderPath | Out-Null
}

if(!(Test-Path $fullOutputFolderPath)) {
    New-Item -ItemType Directory -Force -Path $fullOutputFolderPath | Out-Null
}

if(!(Test-Path $fullPredefinedArgsPath)) {
    New-Item -ItemType File -Force -Path $fullPredefinedArgsPath | Out-Null
    $content = @()
    $content += "##################################################################################################################"
    $content += "# Predefined argument values for BDSup2Sub.jar"
    $content += "# Use with '-use-predef' argument on the BDSup2Sub-Batch script to load the file"
    $content += "#"
    $content += "# Values set in predefined_args.ini file take precedence over command line arguments on the BDSup2Sub-Batch script,"
    $content += "# if no value for an argument is set in the predefined_args.ini file, then command line arguments take precedence,"
    $content += "# if no value for an argument in either the predefined_args.ini file or on the command line is set,"
    $content += "# BDSup2Sub.jar default values will be used"
    $content += "##################################################################################################################"
    $content += "load-settings="
    $content += "resolution="
    $content += "fps-target="
    $content += "convert-fps="
    $content += "delay="
    $content += "filter="
    $content += "palette-mode="
    $content += "minimum-time="
    $content += "merge-time="
    $content += "move-in="
    $content += "move-out="
    $content += "move-x="
    $content += "crop-y="
    $content += "alpha-crop="
    $content += "scale="
    $content += "export-palette="
    $content += "forced-only="
    $content += "force-all="
    $content += "swap="
    $content += "fix-invisible="
    $content += "palette-file="
    $content += "verbose-output="
    $content += "alpha-thr="
    $content += "lum-low-med-thr="
    $content += "lum-med-hi-thr="
    $content += "language="
    Set-Content -Path $fullPredefinedArgsPath -Value $content
}

$inputFiles = Get-ChildItem "$fullInputFolderPath\*" -Include *.idx,*.xml,*.sup,*.ifo,*.sub

if(!(Test-Path $bdSup2SubFilePath)) {
    "BDSup2Sub.jar not found!",
	"Download here:",
	"https://raw.githubusercontent.com/wiki/mjuhasz/BDSup2Sub/downloads/BDSup2Sub.jar", 
    "or get source code here and compile it yourself:",
    "https://github.com/mjuhasz/BDSup2Sub/releases/tag/5.1.2",
    "Then put the BDSup2Sub.jar file into the root folder of the PowerShell script." `
    | Write-Host -ForegroundColor Red `
} elseif ($inputFiles.Count -eq 0) {
    Write-Host "No compatible files found!" -ForegroundColor Red
} else {
    $jarArgsArray = @()

    if(${use-predef}) {
        if(!(Test-Path $fullPredefinedArgsPath)) {
            "'predefined_args.ini not found!'",
            "Use default settings!" | Write-Host -ForegroundColor Yellow
        } else {
            # Get predefined arguments from predefined_args.ini file
            $predefinedArgs = @{}
            Get-Content $fullPredefinedArgsPath | ForEach-Object `
            -Process { 
                $predefTemp = [Regex]::Split($_,'=');
                if($predefTemp[0].CompareTo("") -ne 0 -and ($predefTemp[0].StartsWith("#") -ne $True)) {
                    $predefinedArgs.Add($predefTemp[0], $predefTemp[1])
                }
            } `

            # Get predefined params and set values from predefined_args.ini file
            $predefinedParams = $MyInvocation.MyCommand.Parameters
            foreach ($param in $predefinedParams.GetEnumerator()) {
                foreach ($arg in $predefinedArgs.GetEnumerator()) {
                    if(($param.Key -eq $arg.Key) -and ($arg.Value -ne "")) {
                        if($param.Value.SwitchParameter -eq $False) {
                            Set-Variable -Name $param.Key -Value $arg.Value
                        } else {
                            $argValue = $arg.Value
                            $bool = [System.Convert]::ToBoolean($argValue)
                            Set-Variable -Name $param.Key -Value $bool
                        }
                    }
                }
            }
        }
    }

    if (${load-settings}) { $jarArgsArray += "-L" }
    if (${resolution}) { $jarArgsArray += "-r${resolution}" }
    if (${fps-target}) { $jarArgsArray += "-T${fps-target}" }
    if (${convert-fps}) { $jarArgsArray += "-C${convert-fps}" }
    if (${delay}) { $jarArgsArray += "-d${delay}" }
    if (${filter}) { $jarArgsArray += "-f${filter}" }
    if (${palette-mode}) { $jarArgsArray += "-p${palette-mode}" }
    if (${minimum-time}) { $jarArgsArray += "-m${minimum-time}" }
    if (${merge-time}) { $jarArgsArray += "-x${merge-time}" }
    if (${move-in}) { $jarArgsArray += "-I${move-in}" }
    if (${move-out}) { $jarArgsArray += "-O${move-out}" }
    if (${move-x}) { $jarArgsArray += "-X${move-x}" }
    if (${crop-y}) { $jarArgsArray += "-c${crop-y}" }
    if (${alpha-crop}) { $jarArgsArray += "-a${alpha-crop}" }
    if (${scale}) { $jarArgsArray += "-S${scale}" }
    if (${export-palette}) { $jarArgsArray += "-P" }
    if (${forced-only}) { $jarArgsArray += "-D" }
    if (${force-all}) { $jarArgsArray += "-F${force-all}" }
    if (${swap}) { $jarArgsArray += "-w" }
    if (${fix-invisible}) { $jarArgsArray += "-i" }
    if (${palette-file}) { $jarArgsArray += "-t${palette-file}" }
    if (${verbose-output}) { $jarArgsArray += "-v" }
    if (${alpha-thr}) { $jarArgsArray += "-A${alpha-thr}" }
    if (${lum-low-med-thr}) { $jarArgsArray += "-M${lum-low-med-thr}" }
    if (${lum-med-hi-thr}) { $jarArgsArray += "-H${lum-med-hi-thr}" }
    if (${language}) { $jarArgsArray += "-l${language}" }
    
    foreach ($file in $inputFiles) {
        $outputPath = Join-Path -Path $fullOutputFolderPath -ChildPath $file.Name
        java -jar BDSup2Sub.jar $jarArgsArray "-o${outputPath}" $file
    }
}

Read-Host -Prompt "${newLine}Press Enter to exit"