# BDSup2Sub Batch PowerShell Script

A simple PowerShell script for batch processing files with [BDSup2Sub](https://github.com/mjuhasz/BDSup2Sub)

## Requirements
**BDSup2Sub-Batch PowerShell Script:**
- **Windows**: Pre-Installed Windows PowerShell (v5.1) or install the lastest PowerShell 7 ([see here](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7))
- **MacOS**: Install latest PowerShell 7 ([see here](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-macos?view=powershell-7))
- **Linux**: Install latest PowerShell 7 ([see here](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-7))

**BDSup2Sub.jar:**  
Install latest [Java SE Development Kit](https://www.oracle.com/java/technologies/javase-downloads.html) by Oracle.  
***Note from BDSup2Sub.jar dev:*** Please note that BDSup2Sub is developed and tested with the official Oracle JRE version 7. Using OpenJDK 7 or Oracle JRE 6 should be possible but has not been extensively tested. OpenJDK 6 is not supported.

- **Linux:**
If you install the JDK with the Linux Debian Package, it can happen that the java executable is not correctly linked to `/usr/bin` (You will get an error message while running the script if the java executable is not recognized). If that happens link it manually with `sudo ln -s /usr/lib/jvm/jdk-14.0.2/bin/java /usr/bin/java`.  
*Note that the `jdk-14.0.2` folder will be named different depending on the version you install.*

## Usage
Download latest releases [here](https://gitlab.com/cannonball1911/bdsup2sub-batch-powershell-script/-/releases) or clone repository/download source code.  

- **Windows:**  
Go into the script root folder and open up:
	- **Windows PowerShell 5.1**  
	```bdsup2sub-batch.ps1 [options]```
	- **PowerShell 7**  
	```pwsh bdsup2sub-batch.ps1 [options]```

- **MacOS:**  
Go into the script root folder and open up:
	- **Terminal & PowerShell 7**  
	```pwsh bdsup2sub-batch.ps1 [options]```

- **Linux:**  
Go into the script root folder and open up:
  - **Terminal**  
  ```pwsh bdsup2sub-batch.ps1 [options]```

## Command line options
```
-use-predef                      Use predefined_args.ini file for argument input.

-load-settings                   Load settings stored in configuration file
                                 even if running in command-line mode.
                                 
-resolution <resolution>         Set resolution to: keep, ntsc=480, pal=576,
                                 720p=720, 1080p=1080, 1440x1080
                                     Default: keep
                                    
-fps-target <fps>            	 Synchronize target frame rate to <fps>.
                                 Predefined values: 24p=23.976, pal or 25p=25,
                                 ntsc or 30p=29.967, keep (preserves the source
                                 fps for BD&XML, else default)
                                     Default: automatic (dumb!).
                                    
-convert-fps <src>,<trg>         Convert frame rate from <src> to <trg>
                                 Supported values: 24p=23.976, 25p=25,
                                 30p=29.970
                                 auto,<trg> detects source frame rate.
                                 
-delay <delay>                   Set delay in ms
                                     Default: 0.0
                                 
-filter <filter>                 Set the filter to use for scaling.
                                 Supported values: bilinear, triangle, bicubic,
                                 bell, b-spline, hermite, lanczos3, mitchell
                                     Default: bilinear
                                 
-palette-mode <mode>             Set palette mode.
                                 Supported values: keep, create, dither
                                     Default: create
                                 
-minimum-time <time>             Set minimum display time in ms.
                                     Default: 500
                                 
-merge-time <time>               Set maximum time difference for merging
                                 subtitles in ms.
                                     Default: 200
                                 
-move-in <ratio,offset>          Move captions inside screen ratio <ratio>, +/-
                                 offset <offset>
                                 
-move-out <ratio,offset>         Move captions outside screen ratio <ratio>,
                                 +/- offset <offset>
                                 
-move-x <pos[,offset]>           Move captions horizontally from specified
                                 position. <pos> may be left, right, center
                                 +/- optional offset <offset> (only if moving
                                 left or right)
                                 
-crop-y <n>                      Crop the upper/lower n lines.
                                     Default: 0
                                 
-alpha-crop <n>                  Set the alpha cropping threshold.
                                     Default: 10
                                 
-scale <x,y>                     Scale captions horizontally and vertically.
                                     Default: 1.0,1.0
                                 
-export-palette                  Export target palette in PGCEdit format.

-forced-only                     Export only forced subtitles (when converting
                                 from BD-SUP).
                                 
-force-all <state>               Set or clear the forced flag for all
                                 subtitles.
                                 Supported values: set, clear
                                 
-swap                            Swap Cr/Cb components when loading a BD/HD-DVD
                                 sup file.
                                 
-fix-invisible                   Fix zero alpha frame palette for SUB/IDX and
                                 SUP/IFO.
                                 
-palette-file <file>             Load palette file for SUB/IDX conversion.
                                 Overrides default palette.

-verbose-output                  Verbose console output mode.
```
The following options only make sense for SUB/IDX or SUP/IFO exports:
```
-alpha-thr <n>              	 Set alpha threshold 0..255 for SUB/IDX
                                 conversion.
                                     Default: 80
                                    
-lum-low-med-thr <n>             Set luminance lo/med threshold 0..255 for
                                 SUB/IDX conversion.
                                     Default: auto
                                    
-lum-med-hi-thr <n>         	 Set luminance med/hi threshold 0..255 for
                                 SUB/IDX conversion.
                                     Default: auto
                                    
-language <language>        	 Set language for SUB/IDX export.
                                     Default: en
```
## predefined_args.ini example:
```
##################################################################################################################
# Predefined argument values for BDSup2Sub.jar
# Use with '-use-predef' argument on the BDSup2Sub-Batch script to load the file
#
# Values set in predefined_args.ini file take precedence over command line arguments on the BDSup2Sub-Batch script,
# if no value for an argument is set in the predefined_args.ini file, then command line arguments take precedence,
# if no value for an argument in either the predefined_args.ini file or on the command line is set,
# BDSup2Sub.jar default values will be used
##################################################################################################################
load-settings=
resolution=720p
fps-target=
convert-fps=24p,30p
delay=
filter=
palette-mode=
minimum-time=
merge-time=
move-in=
move-out=
move-x=
crop-y=
alpha-crop=
scale=
export-palette=
forced-only=
force-all=
swap=
fix-invisible=
palette-file=
verbose-output=True
alpha-thr=
lum-low-med-thr=
lum-med-hi-thr=
language=
```
For more information what values are supported by each argument, see 'Command line options'.

---

BDSup2Sub-Batch PowerShell Script © 2020 by Kai Sackl ([@cannonball1911](https://gitlab.com/cannonball1911))
